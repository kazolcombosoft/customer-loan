import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'dart:io';

// enum LoanStatus { Pending, Accepted, Cancelled, Rejected }

class Loan {
  final String id;
  final String bussinessName;
  final String proprieterName;
  final String address;
  final String mobile;
  final double loanAmount;
  final DateTime loanRequestDate;
  final DateTime loanAcceptDate;
  String loanStatus;

  Loan({
    @required this.id,
    @required this.bussinessName,
    @required this.proprieterName,
    @required this.address,
    @required this.mobile,
    @required this.loanAmount,
    @required this.loanRequestDate,
    this.loanAcceptDate = null,
    @required this.loanStatus,
  });
}

class Loans with ChangeNotifier {
  final String _token;
  final String _userId;
  final String _userType;
  List<Loan> _loans = [
    // Loan(
    //   id: DateTime.now().toIso8601String(),
    //   bussinessName: 'Combosoft Limited',
    //   proprieterName: 'CS Ltd',
    //   address: 'Mirpur DOHS',
    //   mobile: '02158965214',
    //   loanAmount: 25000.00,
    //   loanRequestDate: DateTime.now(),
    //   loanAcceptDate: DateTime.now(),
    //   loanStatus: LoanStatus.Pending,
    // ),
    // Loan(
    //   id: DateTime.now().toIso8601String(),
    //   bussinessName: 'Topbrand Limited',
    //   proprieterName: 'Top Ltd',
    //   address: 'Greenhite',
    //   mobile: '3625798457',
    //   loanAmount: 50000.00,
    //   loanRequestDate: DateTime.now(),
    //   loanAcceptDate: DateTime.now(),
    //   loanStatus: LoanStatus.Success,
    // ),
  ];

  Loans(this._token, this._userId, this._userType, this._loans);

  List<Loan> get loans {
    return [..._loans];
  }

  Future<void> addLoan(Loan loadData) async {
    var url = Uri.parse(
        'https://flutter-loan-app-default-rtdb.firebaseio.com/loans.json');

    final response = await http.post(
      url,
      body: json.encode(
        {
          'creatorId': _userId,
          'bussinessName': loadData.bussinessName,
          'proprieterName': loadData.proprieterName,
          'address': loadData.address,
          'mobile': loadData.mobile,
          'loanAmount': loadData.loanAmount,
          'loanRequestDate': DateTime.now().toIso8601String(),
          'loanAcceptDate': null,
          'loanStatus': 'Pending'
        },
      ),
    );
    _loans.insert(
      0,
      Loan(
        id: jsonDecode(response.body)['name'],
        bussinessName: loadData.bussinessName,
        proprieterName: loadData.proprieterName,
        address: loadData.address,
        mobile: loadData.mobile,
        loanAmount: loadData.loanAmount,
        loanRequestDate: loadData.loanRequestDate,
        loanAcceptDate: null,
        loanStatus: loadData.loanStatus,
      ),
    );
    notifyListeners();
  }

  Future<void> getLoans(String resultType) async {
    final queryParams = _userType == 'admin' && resultType == 'all'
        ? ''
        : '?orderBy="creatorId"&equalTo="$_userId"';
    final url = Uri.parse(
        'https://flutter-loan-app-default-rtdb.firebaseio.com/loans.json$queryParams');
    final res = await http.get(url);
    final decodeResponse = json.decode(res.body) as Map<String, dynamic>;
    final List<Loan> newLoanData = [];
    if (decodeResponse != null)
      decodeResponse.forEach((loanId, loanData) {
        newLoanData.add(
          Loan(
            id: loanId,
            bussinessName: loanData['bussinessName'],
            proprieterName: loanData['proprieterName'],
            address: loanData['address'],
            mobile: loanData['mobile'],
            loanAmount: loanData['loanAmount'],
            loanRequestDate: DateTime.parse(loanData['loanRequestDate']),
            loanAcceptDate: loanData['loanAcceptDate'] != null
                ? DateTime.parse(loanData['loanAcceptDate'])
                : null,
            loanStatus: loanData['loanStatus'],
          ),
        );
      });
    _loans = newLoanData;
    notifyListeners();
  }

  Future<void> deleteRequest(String loanId) async {
    final url = Uri.parse(
        'https://flutter-loan-app-default-rtdb.firebaseio.com/loans/$loanId.json');
    final response = await http.delete(url);
    // print('delete result');
    // print(response.body);
    _loans.removeWhere((ln) => ln.id == loanId);
    notifyListeners();
  }

  Future<void> filterLoan(String status) async {
    final url = Uri.parse(
        'https://flutter-loan-app-default-rtdb.firebaseio.com/loans.json');
    final res = await http.get(url);
    final decodeResponse = json.decode(res.body) as Map<String, dynamic>;
    final List<Loan> newLoanData = [];
    if (decodeResponse != null)
      decodeResponse.forEach((loanId, loanData) {
        if (loanData['loanStatus'] == status)
          newLoanData.add(
            Loan(
              id: loanId,
              bussinessName: loanData['bussinessName'],
              proprieterName: loanData['proprieterName'],
              address: loanData['address'],
              mobile: loanData['mobile'],
              loanAmount: loanData['loanAmount'],
              loanRequestDate: DateTime.parse(loanData['loanRequestDate']),
              loanAcceptDate: loanData['loanAcceptDate'] != null
                  ? DateTime.parse(loanData['loanAcceptDate'])
                  : null,
              loanStatus: loanData['loanStatus'],
            ),
          );
      });
    _loans = newLoanData;
    notifyListeners();
    // _loans.where((loan) => loan.loanStatus == status).toList();
    // notifyListeners();
  }

  Future<void> changeLoanStatus(String loanId, String loanStatus) async {
    final url = Uri.parse(
        'https://flutter-loan-app-default-rtdb.firebaseio.com/loans/$loanId.json');
    try {
      final res = await http.patch(url,
          body: json.encode({
            'loanAcceptDate': DateTime.now().toIso8601String(),
            'loanStatus': loanStatus == 'Accepted' ? 'Active' : loanStatus
          }));
      final ind = _loans.indexWhere((loan) => loan.id == loanId);
      _loans[ind].loanStatus = loanStatus == 'Accepted' ? 'Active' : loanStatus;
      if (res.statusCode >= 400) {
        throw HttpException('Failed');
      }
    } catch (error) {
      throw HttpException('Failed');
    }
    notifyListeners();
  }
}
