import 'dart:io';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class Auth with ChangeNotifier {
  String _userId;
  String _token;
  DateTime _expireIn;
  Map<String, String> _userData = {'displayName': '', 'photoUrl': ''};

  bool get isAuthenticate {
    return token != null;
  }

  String get token {
    return _token ?? null;
  }

  Future<bool> tryAutoLogin() async {
    final prefs = await SharedPreferences.getInstance();
    if (!prefs.containsKey('userData')) {
      return false;
    }
    final info =
        json.decode(prefs.getString('userData')) as Map<String, String>;

    _token = info['token'];
    _userId = info['userId'];
    _userData['displayName'] = info['userType'];
    notifyListeners();
    return true;
  }

  String get userId {
    return _userId;
  }

  Map<String, String> get userData {
    return _userData;
  }

  String get userType {
    return _userData['displayName'];
  }

  Future<void> logout() async {
    _userId = null;
    _token = null;
    _userData = {'displayName': null, 'photoUrl': null};
    final prefs = await SharedPreferences.getInstance();
    prefs.clear();
    notifyListeners();
  }

  Future<void> _authenticate(String email, String password, String type) async {
    final url = Uri.parse(
        'https://identitytoolkit.googleapis.com/v1/$type?key=AIzaSyC7u8BBsuCWjrdl0k1roKmp7Qkv88aae_8');
    final response = await http.post(url,
        body: json.encode({
          'email': email,
          'password': password,
          'returnSecureToken': true,
        }));

    final decodeRes = json.decode(response.body);
    if (decodeRes['error'] != null) {
      throw HttpException(decodeRes['error']['message']);
    }
    _token = decodeRes['idToken'];
    _userId = decodeRes['localId'];

    // notifyListeners();
    // print(decodeRes);

    // final userInfoUrl = Uri.parse(
    //     'https://identitytoolkit.googleapis.com/v1/accounts:lookup?key=AIzaSyC7u8BBsuCWjrdl0k1roKmp7Qkv88aae_8');
    // final userInfoResponse =
    //     await http.post(userInfoUrl, body: {'idToken': token});
    // final decodeUserInfo = json.decode(userInfoResponse.body);
    // Map<String, String> _newUserData = {
    //   'displayName': decodeUserInfo['users'][0]['displayName'],
    //   'photoUrl': decodeUserInfo['users'][0]['photoUrl']
    // };
    // _userData = _newUserData;
    // print('new user data');
    // print(_newUserData);
    getUserInfo();
    notifyListeners();
    // print(decodeUserInfo);
    // print('user Info');
    // print(decodeUserInfo);
  }

  Future<void> signUp(String email, String password) async {
    return _authenticate(email, password, 'accounts:signUp');
  }

  Future<void> login(String email, String password) async {
    return _authenticate(email, password, 'accounts:signInWithPassword');
  }

  Future<void> updateUserInfo(Map<String, String> userInfo) async {
    final url = Uri.parse(
        'https://identitytoolkit.googleapis.com/v1/accounts:update?key=AIzaSyC7u8BBsuCWjrdl0k1roKmp7Qkv88aae_8');
    print(token);
    print(userInfo['displayName']);
    //return;
    final response = await http.post(
      url,
      body: json.encode({
        'idToken': token,
        'displayName': userInfo['displayName'],
        'photoUrl': userInfo['photoUrl'],
        'returnSecureToken': true,
      }),
    );
    final decodeResponse = json.decode(response.body);
    getUserInfo();
  }

  Future<void> getUserInfo() async {
    final url = Uri.parse(
        'https://identitytoolkit.googleapis.com/v1/accounts:lookup?key=AIzaSyC7u8BBsuCWjrdl0k1roKmp7Qkv88aae_8');
    final response = await http.post(url, body: {'idToken': token});
    final decodeUserInfo = json.decode(response.body);
    Map<String, String> _newUserData = {
      'displayName': decodeUserInfo['users'][0]['displayName'],
      'photoUrl': decodeUserInfo['users'][0]['photoUrl']
    };
    _userData = _newUserData;
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(
        'userData',
        json.encode(
            {'token': _token, 'userId': _userId, 'userType': userType}));
    notifyListeners();
  }
}
