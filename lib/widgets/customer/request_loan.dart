import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../provider/loans.dart';
import 'package:customer_loan/screens/loan/customer/all_loan_screen.dart';

class RequestLoan extends StatefulWidget {
  @override
  State<RequestLoan> createState() => _RequestLoanState();
}

class _RequestLoanState extends State<RequestLoan> {
  final _form = GlobalKey<FormState>();
  bool _isLoading = false;
  var _formData = Loan(
    id: null,
    bussinessName: '',
    proprieterName: '',
    address: '',
    mobile: '',
    loanAmount: 0,
    loanRequestDate: null,
    loanStatus: null,
  );

  void snackBarMessage(context, String message, Color color) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(
          message,
          textAlign: TextAlign.center,
        ),
        backgroundColor: color,
        duration: Duration(seconds: 3),
      ),
    );
  }

  Future<void> _submit(context) async {
    final formValidate = _form.currentState.validate();
    if (!formValidate) {
      return;
    }
    final saveInfo = _form.currentState.save();
    setState(() {
      _isLoading = true;
    });
    print(_formData);
    try {
      await Provider.of<Loans>(context, listen: false).addLoan(_formData);
      setState(() {
        _isLoading = false;
      });
      snackBarMessage(context, 'Loan Request Sent Successfully',
          Theme.of(context).primaryColor);
      // ScaffoldMessenger.of(context).showSnackBar(
      //   SnackBar(
      //     content: Text(
      //       'Loan Request Sent Successfully',
      //       textAlign: TextAlign.center,
      //     ),
      //     backgroundColor: Theme.of(context).primaryColor,
      //     duration: Duration(seconds: 3),
      //   ),
      // );
      Navigator.of(context).pushReplacementNamed(AllLoansScreen.routeName);
    } catch (error) {
      print(error);
      setState(() {
        _isLoading = false;
      });
      snackBarMessage(
          context, 'Something Went Wrong', Theme.of(context).errorColor);
      // ScaffoldMessenger.of(context).showSnackBar(
      //   SnackBar(
      //     content: Text(
      //       'Something Went Wrong',
      //       textAlign: TextAlign.center,
      //     ),
      //     backgroundColor: Theme.of(context).errorColor,
      //     duration: Duration(seconds: 3),
      //   ),
      // );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _form,
      child: SingleChildScrollView(
        padding: EdgeInsets.only(left: 8, right: 8),
        child: Column(
          children: [
            TextFormField(
              key: Key('bussinessname'),
              decoration: InputDecoration(labelText: 'Bussiness Name'),
              validator: (value) {
                if (value.isEmpty) {
                  return 'Bussiness name required';
                }
                if (value.length > 50) {
                  return 'Please try a short name';
                }
                return null;
              },
              onSaved: (value) {
                _formData = Loan(
                  id: _formData.id,
                  bussinessName: value,
                  proprieterName: _formData.proprieterName,
                  address: _formData.address,
                  mobile: _formData.mobile,
                  loanAmount: _formData.loanAmount,
                  loanRequestDate: _formData.loanRequestDate,
                  loanStatus: _formData.loanStatus,
                );
              },
            ),
            TextFormField(
              key: Key('proprietername'),
              decoration: InputDecoration(labelText: 'Proprietor Name'),
              validator: (value) {
                if (value.isEmpty) {
                  return 'Proprietor name required';
                }
                if (value.length > 50) {
                  return 'Please try a short name';
                }
                return null;
              },
              onSaved: (value) {
                _formData = Loan(
                  id: _formData.id,
                  bussinessName: _formData.bussinessName,
                  proprieterName: value,
                  address: _formData.address,
                  mobile: _formData.mobile,
                  loanAmount: _formData.loanAmount,
                  loanRequestDate: _formData.loanRequestDate,
                  loanStatus: _formData.loanStatus,
                );
              },
            ),
            TextFormField(
              key: Key('address'),
              decoration: InputDecoration(labelText: 'Address'),
              maxLines: 3,
              keyboardType: TextInputType.multiline,
              validator: (value) {
                if (value.isEmpty) {
                  return 'Address required';
                }
                if (value.length > 100) {
                  return 'Please try a short address';
                }
                return null;
              },
              onSaved: (value) {
                _formData = Loan(
                  id: _formData.id,
                  bussinessName: _formData.bussinessName,
                  proprieterName: _formData.proprieterName,
                  address: value,
                  mobile: _formData.mobile,
                  loanAmount: _formData.loanAmount,
                  loanRequestDate: _formData.loanRequestDate,
                  loanStatus: _formData.loanStatus,
                );
              },
            ),
            TextFormField(
              key: Key('mobilenumber'),
              decoration: InputDecoration(labelText: 'Mobile Number'),
              keyboardType: TextInputType.number,
              validator: (value) {
                if (value.isEmpty) {
                  return 'Mobile Number required';
                }
                if (value.length < 9 || value.length > 15) {
                  return 'Mobile number must be 9-15 digit';
                }
                return null;
              },
              onSaved: (value) {
                _formData = Loan(
                  id: _formData.id,
                  bussinessName: _formData.bussinessName,
                  proprieterName: _formData.proprieterName,
                  address: _formData.address,
                  mobile: value,
                  loanAmount: _formData.loanAmount,
                  loanRequestDate: _formData.loanRequestDate,
                  loanStatus: _formData.loanStatus,
                );
              },
            ),
            TextFormField(
              key: Key('loanamount'),
              decoration: InputDecoration(labelText: 'Loan Amount'),
              keyboardType: TextInputType.number,
              validator: (value) {
                if (value.isEmpty) {
                  return 'Loan Amount required';
                }
                if (int.parse(value) < 1000 || int.parse(value) > 10000000) {
                  return 'Amount must be between 1 thousand to 1 crore';
                }
                return null;
              },
              onSaved: (value) {
                _formData = Loan(
                  id: _formData.id,
                  bussinessName: _formData.bussinessName,
                  proprieterName: _formData.proprieterName,
                  address: _formData.address,
                  mobile: _formData.mobile,
                  loanAmount: double.parse(value),
                  loanRequestDate: _formData.loanRequestDate,
                  loanStatus: _formData.loanStatus,
                );
              },
            ),
            RaisedButton(
              color: Theme.of(context).accentColor,
              onPressed: () {
                _submit(context);
              },
              child: !_isLoading ? Text('Submit') : CircularProgressIndicator(),
            ),
          ],
        ),
      ),
    );
  }
}
