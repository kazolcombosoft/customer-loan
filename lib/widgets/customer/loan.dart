import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';

import 'package:customer_loan/provider/loans.dart' as ln;
import '../../screens/loan/customer/loan_details.dart';

class Loan extends StatefulWidget {
  final ln.Loan loan;
  final bool isAdmin;
  Loan(this.loan, this.isAdmin);

  @override
  State<Loan> createState() => _LoanState();
}

class _LoanState extends State<Loan> {
  bool _isLoading = false;
  bool _isAcceptLoading = false;
  bool _isRejectLoading = false;

  void _snackBarMessage(context, String message, Color color) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(
          message,
          textAlign: TextAlign.center,
        ),
        backgroundColor: color,
        duration: Duration(seconds: 3),
      ),
    );
  }

  Future<void> _deleteLoanRequest(context) async {
    setState(() {
      _isLoading = true;
    });
    try {
      await Provider.of<ln.Loans>(context, listen: false)
          .deleteRequest(widget.loan.id);
      _snackBarMessage(context, 'Successfully Cancelled The Loan Request',
          Theme.of(context).primaryColor);
    } catch (error) {
      _snackBarMessage(
          context, 'Something Went Wrong', Theme.of(context).errorColor);
    }
    setState(() {
      _isLoading = false;
    });
  }

  Future<void> _changeLoanStatus(BuildContext context, String status) async {
    setState(() {
      if (status == 'Accepted') _isAcceptLoading = true;
      if (status == 'Rejected') _isRejectLoading = true;
    });

    try {
      await Provider.of<ln.Loans>(context, listen: false)
          .changeLoanStatus(widget.loan.id, status);
      _snackBarMessage(
          context, '$status The Loan Request', Theme.of(context).primaryColor);
    } catch (error) {
      _snackBarMessage(
          context, 'Something Went Wrong', Theme.of(context).errorColor);
    }
    setState(() {
      if (status == 'Accepted') _isAcceptLoading = false;
      if (status == 'Rejected') _isRejectLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      color: Theme.of(context).accentColor,
      child: InkWell(
        onTap: () {
          Navigator.of(context)
              .pushNamed(LoanDetails.routeName, arguments: widget.loan);
        },
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 5.0, horizontal: 10.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Flexible(
                flex: 2,
                fit: FlexFit.tight,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      widget.loan.bussinessName,
                      style: TextStyle(
                        fontSize: 16,
                      ),
                    ),
                    Text(widget.loan.proprieterName),
                  ],
                ),
              ),
              Flexible(
                flex: 1,
                fit: FlexFit.tight,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(DateFormat('dd/MM/yy hh:mm')
                        .format(widget.loan.loanRequestDate)),
                    if (widget.loan.loanAcceptDate != null)
                      Text(DateFormat.yMMMd()
                          .format(widget.loan.loanAcceptDate)),
                    Text(widget.loan.loanStatus)
                  ],
                ),
              ),
              if (!widget.isAdmin)
                FlatButton(
                  onPressed: () {
                    _deleteLoanRequest(context);
                  },
                  child: _isLoading
                      ? Center(
                          child: CircularProgressIndicator(),
                        )
                      : Text('Delete'),
                  textColor: Colors.white,
                  color: Theme.of(context).errorColor,
                ),
              if (widget.isAdmin)
                Container(
                  child: Column(
                    children: [
                      if (widget.loan.loanStatus != 'Active')
                        FlatButton(
                          key: Key('accept'),
                          onPressed: widget.loan.loanStatus == 'Active'
                              ? () {}
                              : () {
                                  _changeLoanStatus(context, 'Accepted');
                                },
                          child: _isAcceptLoading
                              ? Center(
                                  child: CircularProgressIndicator(
                                    color: Colors.white,
                                  ),
                                )
                              : Text(widget.loan.loanStatus == 'Active'
                                  ? 'Accepted'
                                  : 'Accept'),
                          textColor: Colors.white,
                          color: Theme.of(context).primaryColor,
                        ),
                      if (widget.loan.loanStatus != 'Rejected')
                        FlatButton(
                          key: Key('reject'),
                          onPressed: widget.loan.loanStatus == 'Rejected'
                              ? () {}
                              : () {
                                  _changeLoanStatus(context, 'Rejected');
                                },
                          child: _isRejectLoading
                              ? Center(
                                  child: CircularProgressIndicator(
                                    color: Colors.white,
                                  ),
                                )
                              : Text(widget.loan.loanStatus == 'Rejected'
                                  ? 'Rejected'
                                  : 'Reject'),
                          textColor: Colors.white,
                          color: Theme.of(context).errorColor,
                        ),
                    ],
                  ),
                )
            ],
          ),
        ),
      ),
    );
  }
}
