import 'dart:io';
import 'package:flutter/material.dart';
import 'package:path/path.dart';
import 'package:provider/provider.dart';

import '../../screens/loan/customer/request_loan_screen.dart';
import '../../screens/loan/customer/all_loan_screen.dart';
import '../../screens/loan/admin/admin_loans_screen.dart';
import '../widgets/edit_user.dart';
import '../provider/auth.dart';

class MainDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: [
          Container(
            height: 220,
            width: double.infinity,
            padding: EdgeInsets.all(20),
            color: Theme.of(context).primaryColor,
            alignment: Alignment.center,
            child: Consumer<Auth>(
              builder: (ctx, auth, _) => Column(
                children: [
                  // CircleAvatar(
                  //   backgroundImage: auth != null
                  //       ? auth.userData['photoUrl'] != null
                  //           ? FileImage(File(auth.userData['photoUrl']))
                  //           : NetworkImage(
                  //               'https://i2-prod.mirror.co.uk/incoming/article27521608.ece/ALTERNATES/s615b/0_Lionel-Messi-2.jpg')
                  //       : NetworkImage(
                  //           'https://i2-prod.mirror.co.uk/incoming/article27521608.ece/ALTERNATES/s615b/0_Lionel-Messi-2.jpg'),
                  //   backgroundColor: Theme.of(context).accentColor,
                  //   radius: 40.0,
                  // ),
                  FlatButton(
                    onPressed: () {
                      Navigator.of(context).pushNamed(EditUser.routeName);
                    },
                    child: Text(
                      'Edit',
                      style: TextStyle(fontSize: 20),
                    ),
                    textColor: Colors.green.shade100,
                  ),
                  Text(
                    auth != null ? auth.userData['displayName'] ?? '' : '',
                    style: TextStyle(
                        fontSize: 30,
                        fontWeight: FontWeight.bold,
                        color: Colors.black),
                  ),
                ],
              ),
            ),
          ),
          ListTile(
            leading: Icon(
              Icons.monetization_on_rounded,
              size: 26,
            ),
            title: Text(
              'My Loans',
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
            onTap: () {
              Navigator.pushNamed(context, AllLoansScreen.routeName);
            },
          ),
          ListTile(
            leading: Icon(
              Icons.monetization_on_rounded,
              size: 26,
            ),
            title: Text(
              'Request a loan',
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
            onTap: () {
              Navigator.pushNamed(context, RequestLoanScreen.routeName);
            },
          ),
          Consumer<Auth>(
            builder: (ctx, auth, _) => auth != null && auth.userType == 'admin'
                ? ListTile(
                    leading: Icon(
                      Icons.monetization_on_rounded,
                      size: 26,
                    ),
                    title: Text(
                      'Admin All Loans',
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                    onTap: () {
                      Navigator.pushNamed(context, AdminLoansScreen.routeName);
                    },
                  )
                : SizedBox(
                    height: 1,
                  ),
          ),
          ListTile(
            leading: Icon(
              Icons.exit_to_app,
              size: 26,
            ),
            title: Text(
              'Logout',
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
            onTap: () {
              Navigator.of(context).pop();
              Provider.of<Auth>(context, listen: false).logout();
            },
          ),
        ],
      ),
    );
  }
}
