import 'dart:io';

import 'package:customer_loan/screens/loan/admin/admin_loans_screen.dart';
import 'package:customer_loan/screens/loan/customer/all_loan_screen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart' as systemPath;
import 'package:path/path.dart' as path;

import '../provider/auth.dart';

class EditUser extends StatefulWidget {
  static const routeName = '/edit-user';

  @override
  State<EditUser> createState() => _EditUserState();
}

class _EditUserState extends State<EditUser> {
  Map<String, String> _userInfo = {'displayName': '', 'photoUrl': ''};
  var _userNameController = TextEditingController();
  String _userImageUrl;
  File _imageFile;
  bool _isLoading = false;

  Future<void> _showModalMessage(
    BuildContext context,
    String titleMsg,
    String btnTxt,
  ) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(titleMsg),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text(
                btnTxt,
                style: TextStyle(color: Theme.of(context).errorColor),
              ),
            ),
          ],
        );
      },
    );
  }

  Future<void> _updateUserInfo(BuildContext context) async {
    final userName = _userNameController.text;
    if (userName.isEmpty) {
      _showModalMessage(context, 'Please Give User Name', 'DISMISS');
      return;
    }
    setState(() {
      _isLoading = true;
    });
    _userInfo['displayName'] = userName;
    _userInfo['photoUrl'] = _userImageUrl;
    await Provider.of<Auth>(context, listen: false).updateUserInfo(_userInfo);
    setState(() {
      _isLoading = false;
    });
    final userType = Provider.of<Auth>(context, listen: false).userType;
    if (userType == 'admin') {
      Navigator.of(context).pushNamed(AdminLoansScreen.routeName);
    } else {
      Navigator.of(context).pushNamed(AllLoansScreen.routeName);
    }
  }

  Future<void> _selectUserImage() async {
    final imageSourceFile =
        await ImagePicker().pickImage(source: ImageSource.camera);
    setState(() {
      _imageFile = File(imageSourceFile.path);
    });
    final appDir = await systemPath.getApplicationDocumentsDirectory();
    final fileName = path.basename(imageSourceFile.path);
    final saveImage =
        await File(imageSourceFile.path).copy('${appDir.path}/$fileName');
    _userImageUrl = saveImage.path;
    print(saveImage);
    print(_imageFile);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Edit User'),
      ),
      body: Padding(
        padding: EdgeInsets.all(20),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Container(
                  height: 100,
                  width: 100,
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.black, width: 2),
                  ),
                  child: _imageFile != null
                      ? Image.file(
                          _imageFile,
                          fit: BoxFit.cover,
                        )
                      : null,
                ),
                RaisedButton(
                  onPressed: _selectUserImage,
                  child: Text('Choose Photo'),
                  color: Theme.of(context).accentColor,
                ),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            TextField(
              controller: _userNameController,
              decoration: InputDecoration(
                labelText: 'Enter User Name',
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            RaisedButton(
              onPressed: () => _updateUserInfo(context),
              child: _isLoading
                  ? CircularProgressIndicator(
                      color: Colors.white,
                    )
                  : Text('Update'),
              color: Theme.of(context).primaryColor,
              textColor: Colors.white,
            ),
          ],
        ),
      ),
    );
  }
}
