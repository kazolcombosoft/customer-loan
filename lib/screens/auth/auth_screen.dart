import 'dart:io';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../provider/auth.dart';

enum AuthMode { SignUp, Login }

class AuthScreen extends StatefulWidget {
  @override
  State<AuthScreen> createState() => _AuthScreenState();
}

class _AuthScreenState extends State<AuthScreen> {
  AuthMode _authMode = AuthMode.Login;
  final _formKey = GlobalKey<FormState>();
  Map<String, String> _credentialData = {'email': '', 'password': ''};
  final _passwordController = TextEditingController();
  bool _isLoading = false;

  void _changeAuthMode() {
    setState(() {
      if (_authMode == AuthMode.Login) {
        _authMode = AuthMode.SignUp;
      } else {
        _authMode = AuthMode.Login;
      }
    });
  }

  void _showSnacbarMsg(String msg, Color color) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(
          msg,
          textAlign: TextAlign.center,
        ),
        backgroundColor: color,
        duration: Duration(seconds: 3),
      ),
    );
  }

  Future<void> _submit() async {
    final validationResult = _formKey.currentState.validate();
    if (!validationResult) {
      return;
    }
    final submitData = _formKey.currentState.save();
    setState(() {
      _isLoading = true;
    });
    try {
      if (_authMode == AuthMode.SignUp) {
        await Provider.of<Auth>(context, listen: false)
            .signUp(_credentialData['email'], _credentialData['password']);
        //_showSnacbarMsg(context, 'Sing Up Successfull', Colors.green);
      } else {
        await Provider.of<Auth>(context, listen: false)
            .login(_credentialData['email'], _credentialData['password']);
        //_showSnacbarMsg(context, 'Sing In Successfull', Colors.green);
      }
      setState(() {
        _isLoading = false;
      });
    } on HttpException catch (error) {
      print(error);
      //_showSnacbarMsg(context, error.message, Colors.red);
      setState(() {
        _isLoading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.green.shade100,
      body: Container(
        padding: EdgeInsets.all(20),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                _authMode == AuthMode.Login ? 'Login' : 'SignUp',
                style: TextStyle(fontSize: 40, fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 50,
              ),
              Form(
                key: _formKey,
                child: Column(
                  children: [
                    TextFormField(
                      decoration: InputDecoration(
                        prefixIcon: Icon(Icons.email),
                        labelText: 'Enter Email',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                      keyboardType: TextInputType.emailAddress,
                      validator: (value) {
                        if (value.isEmpty || !value.contains('@')) {
                          return 'Enter Valid Email';
                        }
                        return null;
                      },
                      onSaved: (value) {
                        _credentialData['email'] = value;
                      },
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    TextFormField(
                      obscureText: true,
                      decoration: InputDecoration(
                        prefixIcon: Icon(Icons.lock),
                        labelText: 'Enter Password',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                      controller: _passwordController,
                      keyboardType: TextInputType.text,
                      validator: (value) {
                        if (value.isEmpty || value.length < 6) {
                          return 'Enter Valid Password';
                        }
                        return null;
                      },
                      onSaved: (value) {
                        _credentialData['password'] = value;
                      },
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    if (_authMode == AuthMode.SignUp)
                      TextFormField(
                        obscureText: true,
                        decoration: InputDecoration(
                          prefixIcon: Icon(Icons.lock),
                          labelText: 'Confirm Password',
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                        keyboardType: TextInputType.text,
                        validator: _authMode == AuthMode.SignUp
                            ? (value) {
                                if (value.isEmpty ||
                                    value != _passwordController.text) {
                                  return 'Confirm Password Field Not Valid';
                                }
                                return null;
                              }
                            : null,
                      ),
                    if (_authMode == AuthMode.SignUp)
                      SizedBox(
                        height: 20,
                      ),
                    RaisedButton(
                      onPressed: () => _submit(),
                      child: _isLoading
                          ? CircularProgressIndicator(
                              color: Colors.white,
                            )
                          : Text(_authMode == AuthMode.Login
                              ? 'Login'
                              : 'Sign Up'),
                      textColor: Colors.white,
                      color: Colors.green,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(_authMode == AuthMode.Login
                            ? 'Not sign up yet ? '
                            : 'Already have an account ?'),
                        FlatButton(
                          onPressed: _changeAuthMode,
                          child: Text(
                            _authMode == AuthMode.Login ? 'Sign Up' : 'Login',
                            style: Theme.of(context).textTheme.headline6,
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
