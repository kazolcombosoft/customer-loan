import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../widgets/main_drawer.dart';

import '../../../widgets/customer/loan.dart' as c_loan;
import '../../../provider/loans.dart';

class AdminLoansScreen extends StatefulWidget {
  static const routeName = '/admin-loans';

  @override
  State<AdminLoansScreen> createState() => _AllLoansScreenState();
}

class _AllLoansScreenState extends State<AdminLoansScreen> {
  var _isLoading = false;
  Future<void> _filterLoan(context, String status) async {
    await Provider.of<Loans>(context, listen: false).filterLoan(status);
  }

  @override
  Widget build(BuildContext context) {
    // print('in the all loan screen');
    // final allLoans = Provider.of<Loans>(context, listen: false).loans;
    // print(allLoans);
    return Scaffold(
      drawer: MainDrawer(),
      appBar: AppBar(
        title: const Text('All Loans'),
        actions: [
          PopupMenuButton(
            onSelected: (value) {
              _filterLoan(context, value);
            },
            itemBuilder: (ctx) => [
              PopupMenuItem(
                child: Text('Active'),
                value: 'Active',
                // onTap: () {
                //   _filterLoan(context, 'Active');
                // },
              ),
              PopupMenuItem(
                child: Text('Pending'),
                value: 'Pending',
                // onTap: () {
                //   _filterLoan(context, 'Pending');
                // },
              ),
              PopupMenuItem(
                child: Text('Rejected'),
                value: 'Rejected',
                // onTap: () {
                //   _filterLoan(context, 'Rejected');
                // },
              ),
            ],
          ),
        ],
      ),
      // floatingActionButtonLocation: FloatingActionButtonLocation.endDocked,
      // floatingActionButton: FloatingActionButton(
      //   onPressed: () {},
      //   child: Icon(Icons.add),
      //   backgroundColor: Colors.green.shade100,
      // ),
      // bottomNavigationBar: BottomAppBar(
      //   notchMargin: 8.0,
      //   shape: CircularNotchedRectangle(),
      //   color: Colors.green.shade100,
      //   child: Row(
      //     mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      //     children: [
      //       Icon(Icons.pause),
      //       Icon(Icons.stop),
      //       Icon(Icons.access_time),
      //       Padding(padding: EdgeInsets.all(30))
      //     ],
      //   ),
      // ),
      body: FutureBuilder(
        future: Provider.of<Loans>(context, listen: false).getLoans('all'),
        builder: (ctx, dataSnapshot) {
          if (dataSnapshot.connectionState == ConnectionState.waiting) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else {
            if (dataSnapshot.error != null) {
              print(dataSnapshot.error);
              return Text('Error Occured');
            }
            return Consumer<Loans>(
              builder: (ctx, loanData, child) => loanData.loans.isEmpty
                  ? Center(
                      child: Text(
                        'No Data Found',
                      ),
                    )
                  : ListView.builder(
                      itemCount: loanData.loans.length,
                      itemBuilder: (ctx, i) {
                        return c_loan.Loan(loanData.loans[i], true);
                      },
                    ),
            );
          }
        },
      ),
    );
  }
}
