import 'package:customer_loan/provider/loans.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';

class LoanDetails extends StatelessWidget {
  static const routeName = '/single-loan';

  Widget loanRowInfo(context, columnName, value) {
    return Card(
      elevation: 5,
      child: Container(
        padding: EdgeInsets.all(8),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Flexible(
              flex: 1,
              fit: FlexFit.tight,
              child: Text(
                columnName,
                style: TextStyle(fontSize: 18),
              ),
            ),
            Flexible(
              flex: 2,
              fit: FlexFit.tight,
              child: Text(
                value,
                style: TextStyle(fontSize: 16),
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final loanInfo = ModalRoute.of(context).settings.arguments as Loan;
    return Scaffold(
      appBar: AppBar(
        title: Text('Loan Detail Page'),
      ),
      body: Container(
        padding: EdgeInsets.all(10),
        child: Column(
          children: [
            loanRowInfo(context, 'Bussiness Name', loanInfo.bussinessName),
            loanRowInfo(context, 'Proprieter Name', loanInfo.proprieterName),
            loanRowInfo(context, 'Address', loanInfo.address),
            loanRowInfo(context, 'Mobile', loanInfo.mobile),
            loanRowInfo(context, 'Amount', loanInfo.loanAmount.toString()),
            loanRowInfo(context, 'Request Date',
                DateFormat('dd/MM/yy hh:mm').format(loanInfo.loanRequestDate)),
            loanRowInfo(
                context,
                'Accept Date',
                loanInfo.loanAcceptDate != null
                    ? DateFormat.yMMMd().format(loanInfo.loanAcceptDate)
                    : ''),
            loanRowInfo(context, 'Status', loanInfo.loanStatus),
          ],
        ),
      ),
    );
  }
}
