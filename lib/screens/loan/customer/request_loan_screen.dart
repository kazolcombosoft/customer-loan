import 'package:flutter/material.dart';

import '../../../widgets/customer/request_loan.dart';
import 'package:customer_loan/widgets/main_drawer.dart';

class RequestLoanScreen extends StatelessWidget {
  static const routeName = '/request-loan';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Loan Form'),
      ),
      drawer: MainDrawer(),
      body: RequestLoan(),
    );
  }
}
