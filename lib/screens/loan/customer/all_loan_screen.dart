import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../widgets/main_drawer.dart';

import '../../../widgets/customer/loan.dart' as c_loan;
import '../../../provider/loans.dart';

class AllLoansScreen extends StatefulWidget {
  static const routeName = '/all-loan-screen';

  @override
  State<AllLoansScreen> createState() => _AllLoansScreenState();
}

class _AllLoansScreenState extends State<AllLoansScreen> {
  var _isLoading = false;
  Future<void> _filterLoan(context, String status) async {
    await Provider.of<Loans>(context, listen: false).filterLoan(status);
  }

  @override
  Widget build(BuildContext context) {
    // print('in the all loan screen');
    // final allLoans = Provider.of<Loans>(context, listen: false).loans;
    // print(allLoans);
    return Scaffold(
      drawer: MainDrawer(),
      appBar: AppBar(
        title: const Text('My Loans'),
        actions: [
          PopupMenuButton(
            onSelected: (value) {
              _filterLoan(context, value);
            },
            itemBuilder: (ctx) => [
              PopupMenuItem(
                child: Text('Active'),
                value: 'Active',
                // onTap: () {
                //   _filterLoan(context, 'Active');
                // },
              ),
              PopupMenuItem(
                child: Text('Pending'),
                value: 'Pending',
                // onTap: () {
                //   _filterLoan(context, 'Pending');
                // },
              ),
              PopupMenuItem(
                child: Text('Rejected'),
                value: 'Rejected',
                // onTap: () {
                //   _filterLoan(context, 'Rejected');
                // },
              ),
            ],
          ),
        ],
      ),
      body: FutureBuilder(
        future: Provider.of<Loans>(context, listen: false).getLoans('user'),
        builder: (ctx, dataSnapshot) {
          if (dataSnapshot.connectionState == ConnectionState.waiting) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else {
            if (dataSnapshot.error != null) {
              return Text('Error Occured');
            }
            return Consumer<Loans>(
              builder: (ctx, loanData, child) => loanData.loans.isEmpty
                  ? Center(
                      child: Text(
                        'No Data Found',
                      ),
                    )
                  : ListView.builder(
                      itemCount: loanData.loans.length,
                      itemBuilder: (ctx, i) {
                        print(loanData.loans[i]);
                        return c_loan.Loan(loanData.loans[i],false);
                      },
                    ),
            );
          }
        },
      ),
      // body: ListView.builder(
      //   itemCount: allLoans.length,
      //   itemBuilder: (context, i) {
      //     return InkWell(
      //       onTap: () {
      //         Navigator.of(context).pushNamed(LoanDetails.routeName);
      //       },
      //       child: c_loan.Loan(allLoans[i]),
      //     );
      //   },
      // ),
      // (
      //   padding: EdgeInsets.all(10),
      //   children: [
      //     InkWell(
      //       onTap: () {
      //         Navigator.of(context).pushNamed(LoanDetails.routeName);
      //       },
      //       child: c_loan.Loan(),
      //     )
      //     // ListTile(
      //     //   title: Text('Loan request date'),
      //     //   subtitle: Text('Loan accept date'),
      //     //   trailing: RaisedButton(
      //     //     onPressed: () {},
      //     //     child: Text('Cancel'),
      //     //   ),
      //     // ),
      //   ],
      // ),
    );
  }
}
