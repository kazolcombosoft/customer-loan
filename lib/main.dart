import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import './screens/loan/customer/all_loan_screen.dart';
import './screens/loan/customer/request_loan_screen.dart';
import './screens/loan/customer/loan_details.dart';
import './screens/loan/admin/admin_loans_screen.dart';
import './provider/loans.dart';
import './screens/auth/auth_screen.dart';
import './provider/auth.dart';
import './widgets/edit_user.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider(
            create: (ctx) => Auth(),
          ),
          // ChangeNotifierProvider(
          //   create: (ctx) => Loans(),
          // ),
          ChangeNotifierProxyProvider<Auth, Loans>(
            update: (context, auth, previousLoans) => Loans(
                auth.token,
                auth.userId,
                auth.userType,
                previousLoans == null ? [] : previousLoans.loans),
          ),
        ],
        child: Consumer<Auth>(
          builder: (ctx, auth, _) => MaterialApp(
            debugShowCheckedModeBanner: false,
            theme: ThemeData(
              primarySwatch: Colors.green,
              accentColor: Colors.amber,
              //canvasColor: Colors.lightGreen,
            ),
            title: 'Customer Loan',
            //home: AdminLoansScreen(),
            home: auth.isAuthenticate
                ? ((auth.userType == 'admin')
                    ? AdminLoansScreen()
                    : AllLoansScreen())
                : FutureBuilder(
                    future: auth.tryAutoLogin(),
                    builder: (ctx, buildSnapshot) =>
                        buildSnapshot.connectionState == ConnectionState.waiting
                            ? Center(
                                child: CircularProgressIndicator(),
                              )
                            : AuthScreen(),
                  ),
            routes: {
              AdminLoansScreen.routeName: (ctx) => AdminLoansScreen(),
              AllLoansScreen.routeName: (ctx) => AllLoansScreen(),
              RequestLoanScreen.routeName: (ctx) => RequestLoanScreen(),
              LoanDetails.routeName: (ctx) => LoanDetails(),
              EditUser.routeName: (ctx) => EditUser(),
            },
          ),
        ));
    // return MaterialApp(
    //   debugShowCheckedModeBanner: false,
    //   theme: ThemeData(
    //     primarySwatch: Colors.green,
    //     accentColor: Colors.amber,
    //   ),
    //   title: 'Customer Loan',
    //   home: AllLoansScreen(),
    //   routes: {
    //     RequestLoanScreen.routeName: (ctx) => RequestLoanScreen(),
    //     LoanDetails.routeName: (ctx) => LoanDetails(),
    //   },
    // );
  }
}
